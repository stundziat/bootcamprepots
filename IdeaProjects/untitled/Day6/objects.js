"use strict";

(function()
{

    "use strict";
    let student1 = {name: "Joe", gender: "M", gpa: 3.6};
    console.log(student1);

    // Class constructor
    function Student(name, gender, gpa)
    {
        this.name = name;
        this.gender = gender;
        this.gpa = gpa;
    }

    let student2 = new Student("Susan", "F", 3.5);
    let student3 = new Student("Bob", "M", 1.5);
    let student4 = new Student("Alice", "F", 2.5);
    let classroom = [student2, student3, student4];

    console.log(classroom);

    let attendance = classroom.map(student => student.name);
    console.log(attendance);

    let average = (classroom.reduce((sum, student) => sum + student.gpa, 0)) / classroom.length;
    console.log(average);

    let failing = classroom.filter(student => student.gpa < 2.0);
    console.log(failing);

})();