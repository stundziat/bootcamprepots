"use strict";

// let list = [];
// list.length = 3;
// console.log("List length: ", list.length);
// console.log(list[0]);
// console.log("Adding elements");
// list[0] = 1;
// list[1] = 'a';
// list[2] = 3;
// console.log(list);
// list.push('b');
// console.log(list);
//
// console.log(list.slice(1, 3));
// console.log(list.slice(2));
// list = list.slice(1);
// console.log(list);
//
// list.join('1', 2);
// console.log(list);

// 1) Create a week array

let days_in_week = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];
console.log("Days of week: ", days_in_week);

// 2) Add Sunday to start of array

days_in_week.unshift("Sunday");
console.log("Days of week: ", days_in_week);

// 3) Add Saturday to end of array

days_in_week.push("Saturday");
console.log("Days of week: ", days_in_week);

// 4) Remove monday from the array

days_in_week.splice(1, 1);
console.log("Days of week: ", days_in_week);

// 5) Split the array in half

let first_half_week = days_in_week.slice(0, 3);
let second_half_week = days_in_week.slice(3, 6);
console.log("First Half of Week: ", first_half_week);
console.log("Second Half of Week: ", second_half_week);

// 6) Create the job's project string!

let second_word = "Hatter";
let first_word = "Mad";
let project_name = first_word.concat(" ", second_word);
console.log("Project Name: ", project_name);

// 7) Separate the project name back to normal

let words = project_name.split(" ");
console.log("Project Name Words: ", words);

// 8) Find the letter at a given index

let char_a = words[0].substring(1, 2);
console.log("project_name char at 2: ", char_a);
