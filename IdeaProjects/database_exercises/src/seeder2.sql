
# Advanced MySQL Queries

# 1) ***** Aggregate and Math Exercises *****

# a) Select the city with the highest population in a country

SELECT cit.Population, cntry.Name FROM city AS cit
INNER JOIN country AS cntry on cit.CountryCode = cntry.Code
ORDER BY cit.Population DESC;

# b) Select the city with the lowest population in the same country

SELECT cit.Name, cit.Population FROM city as cit
WHERE cit.Population IN
(SELECT MIN(cit.Population) FROM city AS cit
WHERE cit.CountryCode = 'USA');

# c) Return the country and the population difference between
#    the most populated and least populated city

SELECT cit.CountryCode, (MAX(cit.Population) - MIN(cit.Population)) AS difference FROM city AS cit
GROUP BY cit.CountryCode
ORDER BY cit.CountryCode;

# d) Return the total population of the country

SELECT cit.CountryCode, c.Name, SUM(cit.Population) AS summation FROM city AS cit
INNER JOIN country c on cit.CountryCode = c.Code
GROUP BY cit.CountryCode
ORDER BY cit.CountryCode;


# 2) ***** SQL Drill 1 *****

# a) Select the country that speaks the most languages

SELECT c.Name, COUNT(cl.Language) AS num_language FROM countrylanguage AS cl
INNER JOIN country c on cl.CountryCode = c.Code
GROUP BY cl.CountryCode
ORDER BY num_language DESC
LIMIT 1;


# b) Select the most populous country that speaks a language

SELECT c.Name, c.Population FROM countrylanguage AS cl
INNER JOIN country c on cl.CountryCode = c.Code
WHERE cl.Language = 'Spanish'
ORDER BY c.Population DESC
LIMIT 1;


# c) Select the largest foreign city that speaks a language, e.g., for French
#    outside France

SELECT c.Name, cl.Language, cl.Percentage, c.Population FROM countrylanguage AS cl
INNER JOIN country c on cl.CountryCode = c.Code
WHERE cl.IsOfficial = 'F' AND cl.Language = 'English'
ORDER BY c.Population DESC
LIMIT 1;


# d) Select the top ten spoken languages

SELECT DISTINCT cl.CountryCode, cl.Language, cl.Percentage FROM countrylanguage AS cl
ORDER BY cl.Percentage DESC
LIMIT 10;


# 3) ***** SQL Drill 2 *****

# a) Select the country language that is spoken in the most regions

SELECT c.Region, COUNT(c.Region) AS num_regions FROM country as c
INNER JOIN countrylanguage cl on c.Code = cl.CountryCode
WHERE cl.Language = 'Spanish'
GROUP BY c.Region
ORDER BY num_regions DESC
LIMIT 1;


# b) Return the number of people who speak the languages in a city ordered by size

SELECT cit.Name, (cl.Percentage * cit.Population) AS num_speaking FROM city as cit
INNER JOIN countrylanguage cl on cit.CountryCode = cl.CountryCode
WHERE cl.Language = 'English';

# c) Return an ordered list of the countries population per city

SELECT cnt.Name AS country_name, c.Name AS city_name, c.Population
FROM country AS cnt
INNER JOIN city c on cnt.Code = c.CountryCode
ORDER BY country_name;


# 4) SQL Drill 3

# a) Select the largest population country

SELECT cntr.Name, MAX(cntr.Population) AS highest_population FROM country AS cntr
GROUP BY cntr.Name
ORDER BY highest_population DESC
LIMIT 1;

# b) Return an ordered list of the least populated countries

SELECT cntr.Name, MAX(cntr.Population) AS highest_population FROM country AS cntr
GROUP BY cntr.Name
ORDER BY highest_population;

# c) Return the limited list of the low population countries that can fit
#    inside of the large population country

SELECT cntr.Name, cntr.Population
          ,(SELECT SUM(c.Population)
           FROM country AS c
           WHERE c.Population < cntr.Population) AS temp
FROM country AS cntr
HAVING temp < (SELECT MAX(cntr2.Population) FROM country as cntr2)
ORDER BY cntr.Population;


# 5) ***** SQL Drill 4 *****

# a) Return the country with the most cities

SELECT cnt.Name, COUNT(c.Name) AS num_cit FROM country as cnt
INNER JOIN city c on cnt.Code = c.CountryCode
GROUP BY cnt.Name
ORDER BY num_cit DESC
LIMIT 1;

# b) Return the average number of cities a country has



# ***** Advanced *****
SELECT c2.Name
     ,c2.Population
     ,(SELECT sum(c1.Population)
       FROM country c1
       WHERE c1.Population <= c2.Population) as cum_sum
FROM country c2
HAVING cum_sum < (SELECT Population
                  FROM country
                  WHERE name = 'China')
ORDER BY c2.Population;


SELECT *
FROM(SELECT Name
          ,Population
          ,(@runtot :=  Population  + @runtot) AS rt
     FROM country
            JOIN (SELECT @runtot:=0) c
     ORDER BY Population) AS sub1
WHERE sub1.rt < (SELECT Population
                 FROM country
                 WHERE name = 'China')
