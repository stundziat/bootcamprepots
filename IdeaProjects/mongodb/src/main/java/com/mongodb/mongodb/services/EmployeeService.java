package com.mongodb.mongodb.services;

import com.mongodb.mongodb.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService
{
    @Autowired
    private MongoTemplate mongoTemplate;

    public void insertEmployee()
    {
        // Add employee number 1
        Employee employees = new Employee();
        employees.setFirst_name("bat");
        employees.setLast_name("man");
        mongoTemplate.save(employees, "employee");

        // Add employee number 2
        Employee employee_2 = new Employee();
        employee_2.setFirst_name("super");
        employee_2.setLast_name("man");
        mongoTemplate.save(employee_2, "employee");
    }

    public Employee getBat()
    {
        return mongoTemplate.findOne(Query.query(Criteria.where("first_name").is("bat")), Employee.class);
    }

    public Employee getSuperman()
    {
        return mongoTemplate.findOne(Query.query(Criteria.where("first_name").is("super")), Employee.class);
    }
}
